from asyncio.windows_events import NULL
import random

num = random.randint(1, 10)
guess = None

while guess != num:
    guess = input("\nguess a number between 1 and 10: ")
    guess = int(guess)

    if guess == num:
        print("Correct")
        break
    else:
        print("Try again, number is: ")