from secrets import choice

#addition function
def add(x, y):
    return x + y

#subtraction function
def subtract(x, y):
    return x - y

#multiplication function
def multiply(x, y):
    return x * y

#multiplication function
def divide(x, y):
    return x / y

print()
print("Select operation")
print("1.Add")
print("2.Subtract")
print("3.Multiply")
print("4.Divide\n")

while True:
    #input from user
    choice = input("Enter choice(1/2/3/4): ")

    #check if applicable
    if choice in ('1', '2', '3', '4'):
        num1 = float(input("Enter first number: "))
        num2 = float(input("Enter second number: "))

        if choice == '1':
            print(num1, " + ", num2, " = ", add(num1, num2), "\n")

        elif choice == '2':
            print(num1, " - ", num2, " = ", subtract(num1, num2), "\n")

        elif choice == '3':
            print(num1, " * ", num2, " = ", multiply(num1, num2), "\n")

        elif choice == '4':
            print(num1, " / ", num2, " = ", divide(num1, num2), "\n")

        #check if user wants to continue otherwise break the loop
        next_calculation = input("Let's do next calculation? (yes/no): ")
        if next_calculation == "no":
            break

    else:
        print("Invalid Input")